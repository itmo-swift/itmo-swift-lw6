//
//  main.swift
//  lw6
//
//  Created by Daniil Lushnikov on 27.09.2021.
//  Array reversing

// n - amount of elements in array
let n = Int16(readLine() ?? "") ?? 0
var array: [Int] = [Int]()
for _ in 0..<n {
    array.append(Int(readLine() ?? "") ?? 0)
}
// print reversed array
for elem in array.reversed() {
    print(elem)
}
